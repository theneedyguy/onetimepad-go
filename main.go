package main

import (
    //"fmt"
    "os"
    "io/ioutil"
    "crypto/rand"
    "log"
)

func readFile(inputFilePath string) ([]byte){
	content, err := ioutil.ReadFile(inputFilePath)
	if err != nil {
		log.Fatal(err)
	}
	return content
}

func getFileSize(inputFilePath string) int64{
	// read File size from path
	f, err := os.Stat(inputFilePath);
	if err != nil {
		return -1
	}
	size := f.Size()
	return size
}

func generatePad(inputFilePath string) ([]byte){
	pad := make([]byte, getFileSize(inputFilePath))
	if _, err := rand.Read(pad); err != nil {
		log.Fatal(err)
	}
	ioutil.WriteFile("./keyfile", pad, 0644)
	return pad
}

func encrypt(key []byte, file []byte) {
	keySize := len(key)
	var out []byte
	out = make([]byte, keySize)
	for i := 0; i < keySize; i++ {
		out[i] = key[i] ^ file[i]
	}
	ioutil.WriteFile("./output.crypto", out, 0644)
}

func decrypt(key string, file string) {
	inputKey := readFile(key)
	inputFile := readFile(file)

	keySize := len(inputKey)
	var out []byte
	out = make([]byte, keySize)
	for i := 0; i < keySize; i++ {
		out[i] = inputKey[i] ^ inputFile[i]
	}
	ioutil.WriteFile("./output.decrypt", out, 0644)
}

func main() {
	//cipher(generatePad("/root/scripts/go/go-onetimepad/otp.tgz"), readFile("/root/scripts/go/go-onetimepad/otp.tgz"))
	//decrypt("/root/scripts/go/go-onetimepad/keyfile", "/root/scripts/go/go-onetimepad/output.crypto")
	//fmt.Println(generatePad("/root/scripts/go/go-onetimepad/otp.py"))
	//fmt.Println(readFile("/root/scripts/go/go-onetimepad/otp.tgz"))
}
